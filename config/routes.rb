Rails.application.routes.draw do
  devise_for :authors, path: '', path_names: {sign_in: 'login', sign_out: 'logout', sign_up: 'register'}
  root to: "readers/home#index"
  
  get '/blog/:id' => 'readers/posts#show', as: :blog_post 

  scope module: 'authors' do
  	get 'stats' => 'stats#index'
    resources :posts  do
      resources :elements
    end
  end
end
