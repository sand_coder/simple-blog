import ApplicationController from './application_controller';
import Chart from 'chart.js';

/* This is the custom StimulusReflex controller for PublisherReflex.
 * Learn more at: https://docs.stimulusreflex.com
 */
export default class extends ApplicationController  {
	connect(){
		super.connect();
    this.lineChart('page-views','Page views');
		this.lineChart('unique-page-views', 'Unique page view');
	}

  lineChart(target, label){
    var ctx = document.getElementById(target);
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: JSON.parse(ctx.dataset.labels),
        datasets: [{
            label: label,
            data: JSON.parse(ctx.dataset.data),
            borderWidth: 1
        }]
      }
    });
  }
}
