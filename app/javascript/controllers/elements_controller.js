import ApplicationController from './application_controller'
import Sortable from 'sortablejs'

/* This is the custom StimulusReflex controller for PublisherReflex.
 * Learn more at: https://docs.stimulusreflex.com
 */
export default class extends ApplicationController  {
	connect(){
		super.connect();
		let element = document.getElementById('elements')
		if (element != undefined) {
  		Sortable.create(element, { animation: 150});
		}
	}

  sort(){
  	let element = document.getElementById('elements');
  	let element_items =  Array.from(document.getElementsByClassName('element-item'));

  	let elements = element_items.map((element, index)=>{
  		return { id: element.dataset.id, position: index + 1}
  	});

  	element.dataset.elements = JSON.stringify(elements)
  	this.stimulate('ElementsReflex#sort', element);
  }
}
