class ElementsReflex < ApplicationReflex
	def sort
		elements = JSON.parse(element.dataset[:elements])
		elements.each do |element|
			element_record = Element.find(element['id'])
			element_record.update(position: element['position'])
		end

		# NOTE : This is hack! Might break with future versions of stimulusReflex!
		# @halted = true
		morph :nothing
	end
end