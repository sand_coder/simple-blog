class Authors::StatsController < AuthorsController
  def index
  	@count_by_date = PageView.count_by_date(7.days.ago, Date.today)
  	@count_by_date_labels = @count_by_date.keys.to_json
  	@count_by_date_data = @count_by_date.values.to_json

  	@unique_count_by_date = PageView.unique_count_by_date(7.days.ago, Date.today)
  	@unique_count_by_date_labels = @unique_count_by_date.keys.to_json
  	@unique_count_by_date_data = @unique_count_by_date.values.to_json
  end
end
